#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        string = "1 10\n"
        i, j = collatz_read(string)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        var = collatz_eval(1, 10)
        self.assertEqual(var, 20)

    def test_eval_2(self):
        var = collatz_eval(100, 200)
        self.assertEqual(var, 125)

    def test_eval_3(self):
        var = collatz_eval(201, 210)
        self.assertEqual(var, 89)

    def test_eval_4(self):
        var = collatz_eval(900, 1000)
        self.assertEqual(var, 174)

    def test_eval_5(self):
        var = collatz_eval(334368, 345094)
        self.assertEqual(var, 366)

    def test_eval_6(self):
        var = collatz_eval(959293, 987883)
        self.assertEqual(var, 458)

    def test_eval_7(self):
        var = collatz_eval(16218, 573468)
        self.assertEqual(var, 470)

    def test_eval_8(self):
        var = collatz_eval(128075, 191645)
        self.assertEqual(var, 383)

    def test_eval_9(self):
        var = collatz_eval(12778, 781282)
        self.assertEqual(var, 509)

    def test_eval_10(self):
        var = collatz_eval(706183, 787861)
        self.assertEqual(var, 468)

    def test_eval_11(self):
        var = collatz_eval(126568, 473277)
        self.assertEqual(var, 449)

    def test_eval_12(self):
        var = collatz_eval(210000, 219999)
        self.assertEqual(var, 386)

    def test_eval_13(self):
        var = collatz_eval(640000, 649999)
        self.assertEqual(var, 416)

    def test_eval_14(self):
        var = collatz_eval(295988, 344002)
        self.assertEqual(var, 384)
        
    def test_eval_15(self):
        var = collatz_eval(187367, 717090)
        self.assertEqual(var, 509)

    # -----
    # print
    # -----

    def test_print(self):
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(
            write.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
