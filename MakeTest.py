import random
from Collatz import collatz_eval

def main():
	stdin = open("RunCollatz1.in", "r")
	stdout = open("RunCollatz1.out", "w")
	count = 0
	for line in stdin:
		numbers = line.split(' ')
		answer = collatz_eval(int(numbers[0]), int(numbers[1]))
		stdout.write(line[0:-1] + ' ' + str(answer) + '\n')
		print("calculating " + str(count))
		count += 1
	stdin.close()
	stdout.close()
	print("done")

def cache10000():
	stdout = open("RunCollatz1.in", "w") #appends
	i = 1000
	j = 1999
	while j <= 999999:
		stdout.write(str(i) + " " + str(j) + '\n')
		i += 1000
		j += 1000
	stdout.close()

def random100():
	stdout = open("RunCollatz.in", "a")
	for x in range(100):
		i = random.randint(0,1000000)
		j = random.randint(i,1000000)
		stdout.write(str(i) + ' ' + str(j) + '\n')

	stdout.close()
cache10000()
#random100()
main()